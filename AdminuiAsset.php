<?php

namespace azbuco\adminui;

use yii\web\AssetBundle;

class AdminuiAsset extends AssetBundle {

    public $sourcePath = '@azbuco/adminui/assets';
    public $js = [
        'js/adminui.js',
    ];
    public $css = [
        'css/adminui.css',
    ];
    public $publishOptions = [
        'forceCopy' => YII_DEBUG,
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapPluginAsset',
    ];

}
