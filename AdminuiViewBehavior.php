<?php

namespace azbuco\adminui;

class AdminuiViewBehavior extends \yii\base\Behavior {

    public $breadcrumbs = [];
    public $actionbarItems = [];
    public $actionbarLeftItems = [];
    public $actionbarRightItems = [];
    public $actionbarTitle = '';
    public $showBreadcrumbsInActionbar = true;

}
