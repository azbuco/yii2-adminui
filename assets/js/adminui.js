
var App = (function (App, $) {

    App.menu = function (selector, options) {
        var api = {};

        api.defaults = {
        };

        var s = api.settings = $.extend({}, api.defaults, options);

        

        api.toggle = function($el) {
            $el.toggleClass('active');
        };

        function initEvents() {
            $(document).on('click', selector + ' span', function(e) {
                api.toggle($(e.target).closest('li'));
            });
        }
        
        initEvents();

        return api;
    };

    return App;

})(App || {}, jQuery);

App.menu('.adminui-menu');