<?php

namespace azbuco\adminui\widgets;

use azbuco\adminui\AdminuiAsset;
use yii\base\Widget;
use yii\helpers\Html;

class Card extends Widget
{

    /**
     * @var array The HTML attributes for the container tag.
     */
    public $options = [
        'class' => 'mb-4'
    ];

    /**
     * @var string the body content (or use widget as begin - end;
     */
    public $content;

    /**
     * @var string The HTML tag for the header.
     */
    public $headerTag = 'div';

    /**
     * @var array The HTML attributes for the header tag.
     */
    public $headerOptions = [];

    /**
     * @var string The title in the header
     */
    public $title;

    /**
     * @var string The HTML tag for the header.
     */
    public $titleTag = 'h5';

    /**
     * @var array The HTML attributes for the title tag.
     */
    public $titleOptions = [
        'class' => 'my-0',
    ];

    /**
     * @var string The HTML tag for the body. 
     * Set to false if the body has no container (ex. a list box)
     * @see https://getbootstrap.com/docs/4.0/components/card/#list-groups
     */
    public $bodyTag = 'div';

    /**
     * @var array The HTML attributes for the body tag.
     */
    public $bodyOptions = [];

    /**
     * @var string The footer conent
     */
    public $footer;

    /**
     * @var string The HTML tag for the header.
     */
    public $footerTag = 'div';

    /**
     * @var array The HTML attributes for the body tag.
     */
    public $footerOptions = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        if ($this->content === null) {
            ob_start();
        }
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        if ($this->content === null) {
            $this->content = ob_get_clean();
        }

        $this->registerBundle();
        $this->registerClientScript();

        return $this->renderCard($this->content);
    }

    /**
     * Registers plugin and the related events
     */
    protected function registerBundle()
    {
        $view = $this->getView();
        AdminuiAsset::register($view);
    }

    /**
     * Register JS
     */
    protected function registerClientScript()
    {
        
    }

    protected function renderCard($content)
    {
        Html::addCssClass($this->options, ['card']);

        $content = $this->renderHeader()
        . $this->renderBody()
        . $this->renderFooter();

        return Html::tag('div', $content, $this->options);
    }

    protected function renderHeader()
    {
        $title = $this->renderTitle();

        Html::addCssClass($this->headerOptions, 'card-header');

        return empty($title) ? '' : Html::tag($this->headerTag, $title, $this->headerOptions);
    }

    protected function renderTitle()
    {
        if (empty($this->title)) {
            return '';
        }

        return Html::tag($this->titleTag, $this->title, $this->titleOptions);
    }

    protected function renderBody()
    {
        Html::addCssClass($this->bodyOptions, 'card-body');

        return Html::tag($this->bodyTag, $this->content, $this->bodyOptions);
    }

    protected function renderFooter()
    {
        Html::addCssClass($this->footerOptions, 'card-footer');

        return $this->footer ? Html::tag($this->footerTag, $this->footer, $this->footerOptions) : '';
    }

}
