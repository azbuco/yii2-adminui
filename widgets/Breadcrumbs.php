<?php

namespace azbuco\adminui\widgets;

use yii\widgets\Breadcrumbs as YiiBreadcrumbs;

class Breadcrumbs extends YiiBreadcrumbs {

    /**
     * {@inheritdoc}
     */
    public $itemTemplate = "<li class=\"breadcrumb-item\">{link}</li>";

    /**
     * {@inheritdoc}
     */
    public $activeItemTemplate = "<li class=\"breadcrumb-item active\">{link}</li>\n";

}
