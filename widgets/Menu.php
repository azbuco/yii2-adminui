<?php

namespace azbuco\adminui\widgets;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Menu as YiiMenu;

/**
 * based on dmstr/yii2-adminlte-asset/widgets/Menu.php
 */
class Menu extends YiiMenu {

    public $defaultIcon = '';
    public $linkTemplate = '<a href="{url}">{icon} {label}</a>';
    public $labelTemplate = '<span>{icon} {label} {indicator}</span>';
    public $indicator = '<i class="adminui-indicator mdi mdi-chevron-right"></i>';
    
    public function init()
    {
        parent::init();
        Html::addCssClass($this->options, 'adminui-menu');
    }

    protected function renderItem($item)
    {
        if (!isset($item['icon'])) {
            $item['icon'] = $this->defaultIcon;
        }

        if (isset($item['url'])) {
            $template = ArrayHelper::getValue($item, 'template', $this->linkTemplate);

            return strtr($template, [
                '{url}' => Html::encode(Url::to($item['url'])),
                '{label}' => $item['label'],
                '{icon}' => $item['icon'],
            ]);
        }

        $template = ArrayHelper::getValue($item, 'template', $this->labelTemplate);

        return strtr($template, [
            '{label}' => $item['label'],
            '{icon}' => $item['icon'],
            '{indicator}' => $this->indicator,
        ]);
    }

}
