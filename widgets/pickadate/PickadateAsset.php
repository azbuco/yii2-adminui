<?php

namespace azbuco\adminui\widgets\pickadate;

use Yii;
use yii\web\AssetBundle;

class PickadateAsset extends AssetBundle {

    public $sourcePath = '@bower/pickadate/lib/compressed/';
    public $js = [
        'picker.js',
        'picker.date.js',
        'picker.time.js'
    ];
    public $css = [
        //'themes/classic.css',
        //'themes/classic.date.css',
        //'themes/classic.time.css',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
    
    public function init() {
        $siteLanguage = strtolower(Yii::$app->language);
        $languages = \yii\helpers\FileHelper::findFiles(Yii::getAlias('@bower/pickadate/lib/compressed/translations'), ['only' => [$siteLanguage. '*']]);
        if (!empty($languages)) {
            $translation = basename($languages[0]);
            $this->js[] = 'translations/' . $translation;
        }
    }
    

}
