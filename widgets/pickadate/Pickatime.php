<?php

namespace azbuco\adminui\widgets\pickadate;

use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\View;
use yii\widgets\InputWidget;

class Pickatime extends InputWidget {

    public $options = [
        'class' => 'form-control'
    ];
    public $clientOptions = [];
    public $varName = null;

    /**
     * Initializes the widget
     */
    public function init()
    {
        parent::init();
    }

    /**
     * Runs the widget
     *
     * @return string|void
     */
    public function run()
    {
        $this->registerAssets();
        if ($this->hasModel()) {
            echo Html::activeTextInput($this->model, $this->attribute, $this->options);
        } else {
            echo Html::textInput($this->name, $this->value, $this->options);
        }
    }

    /**
     * Register client assets
     */
    public function registerAssets()
    {
        $view = $this->getView();
        PickadateAsset::register($view);
        $options = Json::encode($this->clientOptions, JSON_NUMERIC_CHECK);
        $js = "$('";
        $js .= '#' . $this->options['id'];
        $js .= "').pickatime({$options});";
        if ($this->varName !== null) {
            $js = "var {$this->varName} = {$js}";
        }
        $this->getView()->registerJs($js, View::POS_END);
    }

}
