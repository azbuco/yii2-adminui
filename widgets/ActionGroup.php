<?php

namespace azbuco\adminui\widgets;

use yii\bootstrap\Html;
use yii\bootstrap\Widget;

class ActionGroup extends Widget
{
    public $title = '';
    public $items = [];
    public $size = 'sm';

    public function run()
    {
        Html::addCssClass($this->options, 'actiongroup');
        
        $actions = [];
        foreach($this->items as $item) {
            if (is_array($item)) {
                $content = isset($item['content']) ? $item['content'] : '&nbsp;';
                $options = isset($item['options']) ? $item['options'] : [];
                $options['href'] = isset($item['url']) ? $item['url'] : '#';
                
                Html::addCssClass($options, 'action');
                Html::addCssClass($options, 'action' . $this->size);
                
                $actions[] = Html::tag('a', $content, $options);  
                
            } else {
                $actions[] = $item;
            }
        }
        
        return Html::tag('div', implode("\n", $actions), $this->options);
    }
}
