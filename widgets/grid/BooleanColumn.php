<?php

namespace azbuco\adminui\widgets\grid;

class BooleanColumn extends \yii\grid\DataColumn {

    public $trueValue = '<i class="mdi mdi-radiobox-marked text-info"></i>';
    public $falseValue = '<i class="mdi mdi-radiobox-blank text-muted"></i>';
    public $format = 'raw';

    public function init()
    {
        parent::init();

        \yii\helpers\Html::addCssClass($this->headerOptions, 'column-boolean');
        \yii\helpers\Html::addCssClass($this->contentOptions, 'column-boolean');
        \yii\helpers\Html::addCssClass($this->footerOptions, 'column-boolean');
    }

    protected function renderDataCellContent($model, $key, $index)
    {
        if ($this->content === null) {
            if (boolval($this->getDataCellValue($model, $key, $index)) === true) {
                return $this->trueValue;
            }
            return $this->falseValue;
        }

        return parent::renderDataCellContent($model, $key, $index);
    }

}
