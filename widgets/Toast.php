<?php

namespace azbuco\adminui\widgets;

class Toast extends \yii\bootstrap\Widget
{
    // alert types
    const ALERT_INFO = 'info';
    const ALERT_SUCCESS = 'success';
    const ALERT_WARNING = 'warning';
    const ALERT_DANGER = 'error';
    // widget types
    const TYPE_NOTY = 'noty';
    const TYPE_ALERT = 'alert';

    public $alert_types = [
        'info',
        'success',
        'warning',
        'danger',
        'error',
    ];

    /**
     * @var string Alert type (see alert types)
     */
    public $alert_type = 'info';

    /**
     * @var string Widget type (noty or alert)
     */
    public $type = 'noty';

    /**
     * @var array the options for rendering the close button tag.
     */
    public $closeButton = [];

    /**
     * @var array Noty.js configuration
     * @see https://ned.im/noty/#/options
     */
    public $options = [];

    public function init()
    {
        parent::init();

        $session = \Yii::$app->session;
        $flashes = $session->getAllFlashes();

        foreach ($flashes as $type => $data) {
            if (in_array($type, $this->alert_types)) {
                $data = (array) $data;
                foreach ($data as $i => $message) {
                    if ($this->type === self::TYPE_NOTY) {
                        \azbuco\noty\Noty::widget([
                            'text' => $message,
                            'type' => $type,
                        ]);
                    } else {
                        $this->options['class'] = 'alert-' . $type;
                        $this->options['id'] = $this->getId() . '-' . $type . '-' . $i;

                        echo \yii\bootstrap\Alert::widget([
                            'body' => $message,
                            'closeButton' => $this->closeButton,
                            'options' => $this->options,
                        ]);
                    }
                }

                $session->removeFlash($type);
            }
        }
    }
}
