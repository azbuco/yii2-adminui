<?php

namespace azbuco\adminui\widgets;

use yii\base\Widget;

class ErrorSummary extends Widget {

    public $template = '<div class="alert alert-danger" role="alert">{content}</div>';
   
    /**
     * @var \yii\base\Model[]
     */
    public $models = [];

    public function init()
    {
        parent::init();
        
        if (!is_array($this->models)) {
            $this->models = [
                $this->models,
            ];
        }
    }
    
    public function run()
    {
        $show = false;
        foreach($this->models as $model) {
            if ($model->hasErrors()) {
                $show = true;
                break;
            }
        }
        
        if ($show) {
            return str_replace('{content}', \yii\helpers\Html::errorSummary($this->models), $this->template);
        }

        return '';
    }

}
