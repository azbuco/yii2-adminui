<?php

namespace azbuco\adminui\widgets;

use yii\helpers\Html;
use yii\helpers\Inflector;

class Panel extends Card {

    /**
     * @var array Icons at the header
     */
    public $controls = [];
    
    /**
     *
     * @var array HTML opciók a controlok 
     */
    public $controlsOptions = [];
    public $actions = [];
    public $actionsOptions = [];
    public $collapsible = false;
    public $collapsed = false;
    public $collapseIcon = '<i class="mdi mdi-chevron-up"></i>';
    public $headerLayout = '{title}{actions}{controls}{collapse}';
    
    protected $_collapseId;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->_collapseId = $this->getId() . '-card-body-wrapper';
        
        if ($this->collapsible) {
            Html::addCssClass($this->options, 'card-collapsible');
        }
        
        Html::addCssClass($this->headerOptions, 'd-flex align-items-center');

        parent::init();
    }

    protected function renderPanel($content)
    {
        Html::addCssClass($this->options, 'panel');

        $content = $this->renderHeader()
        . $this->renderBody($content)
        . $this->renderFooter();

        return Html::tag('div', $content, $this->options);
    }

    protected function renderHeader()
    {   
        $header = preg_replace_callback("/{(\\w+)}/", function ($matches) {
            $renderMethod = 'render' . Inflector::id2camel($matches[1]);
            if (!method_exists($this, $renderMethod)) {
                return $matches[0];
            }
            return $this->$renderMethod();
        }, $this->headerLayout);

        Html::addCssClass($this->headerOptions, 'card-header');
        
        return empty($header) ? '' : Html::tag('div', $header, $this->headerOptions);
    }

    protected function renderControls()
    {
        if (empty($this->controls)) {
            return '';
        }
        
        $controls = [];
        foreach($this->controls as $control) {
            $controls[] = Html::tag('div', $control, ['class' => 'card-control mr-1']);
        }

        Html::addCssClass($this->controlsOptions, 'card-controls d-flex align-items-center ml-auto mr-3');
        
        return Html::tag('div', implode("\n", $controls), $this->controlsOptions);
    }

    protected function renderActions()
    {
        if (empty($this->actions)) {
            return '';
        }
        
        $actions = [];
        foreach($this->actions as $action) {
            $actions[] = Html::tag('div', $action, ['class' => 'card-action mr-1']);
        }
        
        Html::addCssClass($this->actionsOptions, 'card-actions d-flex align-items-center ml-3');
        
        return Html::tag('div', implode("\n", $actions), $this->actionsOptions);
    }

    protected function renderCollapse()
    {
        if ($this->collapsible === false) {
            return '';
        }

        $a = Html::a($this->collapseIcon, '#' . $this->_collapseId, [
            'class' => 'collapse-trigger ' . ($this->collapsed ? 'collapsed' : ''),
            'aria-expanded' => $this->collapsed ? 'false' : 'true',
            'aria-controls' => $this->_collapseId,
            'data-toggle' => 'collapse']);
        
        $collapseOptions = empty($this->actions) ? ['class' => 'ml-auto'] : [];
        
        return Html::tag('div', $a, $collapseOptions);
    }

    protected function renderBody()
    {
        $body = parent::renderBody();

        if (!$this->collapsible) {
            return $body;
        }

        $wrapperOptions = [
            'id' => $this->_collapseId,
            'class' => 'collapse'
        ];
        
        if (!$this->collapsed) {
            Html::addCssClass($wrapperOptions, 'show');
        }

        return Html::tag('div', $body, $wrapperOptions);
    }

}
